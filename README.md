Weather
===============
App that provides weather info for list of cities.
It uses the Open Weather Map API to retrieve those info.

How to install
----------
This project requires to install [CocoaPods](https://cocoapods.org/) as well as having Xcode 9.4.
Once both are installed, you will need to open the terminal and do the following:
```
cd Path/to/project
pod install
```
Afterward, you will need to open the file named **Weather.xcworkspace** and then you will be able to run the application.