//
//  WeatherCitySpec.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Weather

class WeatherCitySpec: QuickSpec {
  
  override func spec() {
    describe("A WeatherCity") {
      context("is instantiated") {
        // Given
        let weatherCity: WeatherCity!
        
        // When
        weatherCity = WeatherCityFixtures.defaultInstance()
        
        // Then
        it("should not be nil") {
          expect(weatherCity).toNot(beNil())
        }
      }
      
      context("is decoded") {
        // Given
        let weatherCity: WeatherCity?
        
        // When
        weatherCity = WeatherCityFixtures.defaultInstanceFromJson()
        
        // Then
        it("should be correctly instantiated") {
          expect(weatherCity).toNot(beNil())
          expect(weatherCity?.id).to(equal(2172797))
          expect(weatherCity?.name).to(equal("Cairns"))
          expect(weatherCity?.coordinates.latitude).to(equal(-16.92))
          expect(weatherCity?.coordinates.longitude).to(equal(145.77))
          expect(weatherCity?.main.temperature).to(equal(300.15))
          expect(weatherCity?.main.temperatureMinimum).to(equal(300.15))
          expect(weatherCity?.main.temperatureMaximum).to(equal(300.15))
          expect(weatherCity?.main.temperatureMaximum).to(equal(300.15))
          expect(weatherCity?.main.pressure).to(equal(1007))
          expect(weatherCity?.main.humidity).to(equal(74))
          expect(weatherCity?.main.seaLevel).to(beNil())
          expect(weatherCity?.main.groundLevel).to(beNil())
          expect(weatherCity?.dateReceivingTime).to(equal(1485790200))
          expect(weatherCity?.wind.speed).to(equal(3.6))
          expect(weatherCity?.wind.direction).to(equal(160))
          expect(weatherCity?.snow).to(beNil())
          expect(weatherCity?.rain).to(beNil())
          expect(weatherCity?.clouds.cloudiness).to(equal(40))
          expect(weatherCity?.weather.count).to(equal(1))
          expect(weatherCity?.weather.first?.id).to(equal(802))
          expect(weatherCity?.weather.first?.name).to(equal("Clouds"))
          expect(weatherCity?.weather.first?.description).to(equal("scattered clouds"))
          expect(weatherCity?.weather.first?.icon).to(equal("03n"))
          expect(weatherCity?.sun?.country).to(equal("AU"))
          expect(weatherCity?.sun?.sunrise).to(equal(1485720272))
          expect(weatherCity?.sun?.sunset).to(equal(1485766550))
        }
      }
    }
  }
}
