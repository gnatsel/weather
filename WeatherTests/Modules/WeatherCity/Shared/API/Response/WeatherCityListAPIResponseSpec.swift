//
//  WeatherCityListAPIResponseSpec.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Weather

class WeatherCityListAPIResponseSpec: QuickSpec {
  
  override func spec() {
    describe("A WeatherCityListAPIResponse") {
      context("is instantiated") {
        // Given
        let weatherCityListAPIResponse: WeatherCityListAPIResponse!
        
        // When
        weatherCityListAPIResponse = WeatherCityListAPIResponseFixtures.defaultInstance()
        
        // Then
        it("should not be nil") {
          expect(weatherCityListAPIResponse).toNot(beNil())
        }
      }
      
      context("is decoded") {
        // Given
        let weatherCityListAPIResponse: WeatherCityListAPIResponse?
        
        // When
        weatherCityListAPIResponse = WeatherCityListAPIResponseFixtures.defaultInstanceFromJson()
        
        // Then
        it("should be correctly instantiated") {
          expect(weatherCityListAPIResponse).toNot(beNil())
          expect(weatherCityListAPIResponse?.cod).to(equal("200"))
          expect(weatherCityListAPIResponse?.calctime).to(equal(0.3107))
          expect(weatherCityListAPIResponse?.cnt).to(equal(15))
          expect(weatherCityListAPIResponse?.list.count).to(equal(15))
        }
      }
    }
  }
}
