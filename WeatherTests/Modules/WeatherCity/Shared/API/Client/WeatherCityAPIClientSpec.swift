//
//  WeatherCityAPIClientSpec.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import Quick
import Nimble
import RxSwift
import RxCocoa

@testable import Weather

class WeatherCityAPIClientSpec: QuickSpec {
  
  let baseURL = URL(string: "https://www.example.com")!
  
  override func spec() {
    describe("A WeatherCityAPIClient") {
      context("is instantiated") {
        // Given
        let weatherCityAPIClient: WeatherCityAPIClient!
        
        // When
        weatherCityAPIClient = WeatherCityAPIClient(baseURL: baseURL)
        
        // Then
        it("should not be nil") {
          expect(weatherCityAPIClient).toNot(beNil())
        }
        it("should implement the following protocols") {
          expect(weatherCityAPIClient).to(beAKindOf(WeatherCityAPIClientType.self))
        }
      }
      describe("GET weather city list") {
        context("with a valid json") {
          // Given
          var actualURL: URL?
          let dataFetcher: DataFetching = MockDataFetcher() { request in
            actualURL = request.url?.absoluteURL
            return Observable.create { observer in
              observer.onNext(WeatherCityListAPIResponseFixtures.defaultDataFromJson())
              observer.onCompleted()
              return Disposables.create()
            }
          }
          let weatherCityAPIClient: WeatherCityAPIClient = WeatherCityAPIClient(baseURL: baseURL, dataFetcher: dataFetcher)
          let disposeBag: DisposeBag = DisposeBag()
          var actualWeatherCityArray: [WeatherCity]?
          
          
          // When
          waitUntil(timeout: 1) { done in
            weatherCityAPIClient.getWeatherCityList()
              .subscribe(onNext: { weatherCityArray in
                actualWeatherCityArray = weatherCityArray
                done()
              }).disposed(by: disposeBag)
          }
          
          it("should have its URL set correctly") {
            let expectedURL = URL(string: "https://www.example.com/data/2.5/box/city?bbox=12,32,15,37,10&appid=b1b15e88fa797225412429c1c50c122a1")!
            expect(actualURL).toNot(beNil())
            expect(actualURL).to(equal(expectedURL))
          }
          
          it("should retrieve an array of WeatherCity") {
            expect(actualWeatherCityArray).toNot(beNil())
            expect(actualWeatherCityArray?.count).to(equal(15))
          }
        }
      }
      describe("GET weather city detail") {
        context("with a valid json") {
          // Given
          var actualURL: URL?
          let weatherCity: WeatherCity = WeatherCityFixtures.defaultInstanceFromJson()
          let dataFetcher: DataFetching = MockDataFetcher() { request in
            actualURL = request.url?.absoluteURL
            return Observable.create { observer in
              observer.onNext(WeatherCityFixtures.defaultDataFromJson())
              observer.onCompleted()
              return Disposables.create()
            }
          }
          let weatherCityAPIClient: WeatherCityAPIClient = WeatherCityAPIClient(baseURL: baseURL, dataFetcher: dataFetcher)
          let disposeBag: DisposeBag = DisposeBag()
          var actualWeatherCity: WeatherCity?
          
          
          // When
          waitUntil(timeout: 1) { done in
            weatherCityAPIClient.getWeatherCityDetail(from: weatherCity)
              .subscribe(onNext: { weatherCity in
                actualWeatherCity = weatherCity
                done()
              }).disposed(by: disposeBag)
          }
          
          it("should have its URL set correctly") {
            let expectedURL = URL(string: "https://www.example.com/data/2.5/weather?id=2172797&appid=b1b15e88fa797225412429c1c50c122a1")!
            expect(actualURL).toNot(beNil())
            expect(actualURL).to(equal(expectedURL))
          }
          
          it("should retrieve a WeatherCity") {
            expect(actualWeatherCity).toNot(beNil())
          }
        }
      }
    }
  }
}
