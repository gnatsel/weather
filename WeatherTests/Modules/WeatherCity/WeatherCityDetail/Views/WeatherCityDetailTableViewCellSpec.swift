//
//  WeatherCityDetailTableViewCellSpec.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Weather

class WeatherCityDetailTableViewCellSpec: QuickSpec {
  override func spec() {
    describe("A WeatherCityDetailTableViewCell") {
      context("is configured with a WeatherCityDetailCellViewModel") {
        // Given
        let weatherCityDetailCellViewModel = WeatherCityDetailCellViewModel(infoName: "Title", infoValue: "value")
        let weatherCityDetailTableViewCell = WeatherCityDetailTableViewCell(frame: .zero)
        weatherCityDetailTableViewCell.infoNameLabel = UILabel()
        weatherCityDetailTableViewCell.infoValueLabel = UILabel()
        
        // When
        weatherCityDetailTableViewCell.configureWith(viewModel: weatherCityDetailCellViewModel)
        
        // Then
        it("should configure its labels correctly") {
          expect(weatherCityDetailTableViewCell.infoNameLabel.text).to(equal("Title"))
          expect(weatherCityDetailTableViewCell.infoValueLabel.text).to(equal("value"))
        }
      }
    }
  }
}
