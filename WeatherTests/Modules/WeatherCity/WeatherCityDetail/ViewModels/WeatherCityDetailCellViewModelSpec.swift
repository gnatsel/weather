//
//  WeatherCityDetailCellViewModelSpec.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Weather

class WeatherCityDetailCellViewModelSpec: QuickSpec {
  
  override func spec() {
    describe("A WeatherCityDetailCellViewModel") {
      context("is instantiated") {
        // Given
        let weatherCityDetailCellViewModel: WeatherCityDetailCellViewModel
        
        // When
        weatherCityDetailCellViewModel = WeatherCityDetailCellViewModel(infoName: "Title", infoValue: "value")
        
        // Then
        it("should not be nil") {
          expect(weatherCityDetailCellViewModel).toNot(beNil())
        }
        it("should have its properties correctly instantied") {
          expect(weatherCityDetailCellViewModel.infoName).to(equal("Title"))
          expect(weatherCityDetailCellViewModel.infoValue).to(equal("value"))
        }
      }
    }
  }
}
