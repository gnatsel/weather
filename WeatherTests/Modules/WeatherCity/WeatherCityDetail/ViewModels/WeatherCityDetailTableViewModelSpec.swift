//
//  WeatherCityDetailTableViewModelSpec.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import Quick
import Nimble
import RxCocoa
import RxSwift

@testable import Weather

class WeatherCityDetailTableViewModelSpec: QuickSpec {
  
  override func spec() {
    describe("A WeatherCityDetailTableViewModel") {
      
      context("is instantiated") {
        // Given
        let weatherCity = WeatherCityFixtures.defaultInstance()
        let weatherCityAPIClient = SuccessMockWeatherCityAPIClient()
        let weatherCityDetailTableViewModel: WeatherCityDetailTableViewModel
        
        // When
        weatherCityDetailTableViewModel = WeatherCityDetailTableViewModel(weatherCity: weatherCity, apiClient: weatherCityAPIClient)
        
        // Then
        it("should not be nil") {
          expect(weatherCityDetailTableViewModel).toNot(beNil())
        }
        it("should implement the following protocols") {
          expect(weatherCityDetailTableViewModel).to(beAKindOf(RxTableViewModelRefreshing.self))
          expect(weatherCityDetailTableViewModel).to(beAKindOf(RxErrorAlertViewModelSubjectProvider.self))
        }
      }
      
      describe("getWeatherCityDetailCellViewModels") {
        context("is successfully retrieving a WeatherCity") {
          // Given
          let weatherCity = WeatherCityFixtures.defaultInstanceFromJson()
          let weatherCityAPIClient = SuccessMockWeatherCityAPIClient()
          let weatherCityDetailTableViewModel: WeatherCityDetailTableViewModel = WeatherCityDetailTableViewModel(weatherCity: weatherCity, apiClient: weatherCityAPIClient)
          let disposeBag = DisposeBag()
          
          // When
          var weatherCityDetailCellViewModels: [WeatherCityDetailCellViewModel]!
          waitUntil(timeout: 1) { done in
            weatherCityDetailTableViewModel.getWeatherCityDetailCellViewModels()
              .subscribe(onNext: { cellViewModels in
                weatherCityDetailCellViewModels = cellViewModels
                done()
              }).disposed(by: disposeBag)
          }
          // Then
          it("should generate correctly the cell view models") {
            expect(weatherCityDetailCellViewModels).toNot(beNil())
            expect(weatherCityDetailCellViewModels.count).to(equal(6))
            expect(weatherCityDetailCellViewModels[0].infoName).to(equal("City Info"))
            expect(weatherCityDetailCellViewModels[0].infoValue).to(equal("Cairns\nlatitude: -16.92, longitude: 145.77\ndate received: 31-01-2017 02:30:00 AM"))
            expect(weatherCityDetailCellViewModels[1].infoName).to(equal("Sun"))
            expect(weatherCityDetailCellViewModels[1].infoValue).to(equal("Sunrise: 30-01-2017 07:04:32 AM\nSunset: 30-01-2017 07:55:50 PM"))
            expect(weatherCityDetailCellViewModels[2].infoName).to(equal("Weather parameters"))
            expect(weatherCityDetailCellViewModels[2].infoValue).to(equal("Humidity: 74.0%\nCurrent temperature: 300.2 °C\nMinimum daily temperature: 300.2 °C\nMaximum daily temperature: 300.2 °C\nAtmospheric Pressure: 1007.0 hPa"))
            expect(weatherCityDetailCellViewModels[3].infoName).to(equal("Clouds"))
            expect(weatherCityDetailCellViewModels[3].infoValue).to(equal("scattered clouds"))
            expect(weatherCityDetailCellViewModels[4].infoName).to(equal("Wind"))
            expect(weatherCityDetailCellViewModels[4].infoValue).to(equal("Direction: 160.0 °\nSpeed: 3.6 m/s"))
            expect(weatherCityDetailCellViewModels[5].infoName).to(equal("Cloudiness"))
            expect(weatherCityDetailCellViewModels[5].infoValue).to(equal("40.0%"))
          }
        }
        
        context("is failing retrieving a WeatherCity") {
          // Given
          let weatherCity = WeatherCityFixtures.defaultInstanceFromJson()
          let weatherCityAPIClient = FailureMockWeatherCityAPIClient()
          let weatherCityDetailTableViewModel: WeatherCityDetailTableViewModel = WeatherCityDetailTableViewModel(weatherCity: weatherCity, apiClient: weatherCityAPIClient)
          
          let disposeBag = DisposeBag()
          
          // When
          var getWeatherCityAlertViewModel: AlertViewModel!
          var weatherCityDetailCellViewModels: [WeatherCityDetailCellViewModel]!
          waitUntil(timeout: 1) { done in
            weatherCityDetailTableViewModel.errorAlertViewModelSubject.subscribe(onNext: { alertViewModel in
              getWeatherCityAlertViewModel = alertViewModel
            }).disposed(by: disposeBag)
            weatherCityDetailTableViewModel.getWeatherCityDetailCellViewModels()
              .subscribe(onNext: { cellViewModels in
                weatherCityDetailCellViewModels = cellViewModels
                done()
              }).disposed(by: disposeBag)
          }
          // Then
          it("should send an alertViewModel to be displayed") {
            expect(weatherCityDetailCellViewModels).to(beEmpty())
            expect(getWeatherCityAlertViewModel).toNot(beNil())
            expect(getWeatherCityAlertViewModel.title).to(equal("Retrieve weather detail"))
            expect(getWeatherCityAlertViewModel.body).to(equal("The operation couldn’t be completed. ( error 400.)"))
            expect(getWeatherCityAlertViewModel.confirmTitle).to(equal("OK"))
          }
        }
      }
    }
  }
}
