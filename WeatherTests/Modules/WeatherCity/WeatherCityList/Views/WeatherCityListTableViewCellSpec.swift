//
//  WeatherCityListTableViewCellSpec.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Weather

class WeatherCityListTableViewCellSpec: QuickSpec {
  override func spec() {
    describe("A WeatherCityListTableViewCell") {
      context("is configured with a WeatherCityListCellViewModel") {
        // Given
        let weatherCity: WeatherCity = WeatherCityFixtures.defaultInstanceFromJson()
        let weatherCityListCellViewModel: WeatherCityListCellViewModel = WeatherCityListCellViewModel(weatherCity: weatherCity)
        let weatherCityListTableViewCell = WeatherCityListTableViewCell(frame: .zero)
        weatherCityListTableViewCell.cityLabel = UILabel()
        weatherCityListTableViewCell.temperatureLabel = UILabel()
        
        // When
        weatherCityListTableViewCell.configureWith(viewModel: weatherCityListCellViewModel)
        
        // Then
        it("should configure its labels correctly") {
          expect(weatherCityListTableViewCell.cityLabel.text).to(equal("Cairns"))
          expect(weatherCityListTableViewCell.temperatureLabel.text).to(equal("300.2 °C"))
        }
      }
    }
  }
}
