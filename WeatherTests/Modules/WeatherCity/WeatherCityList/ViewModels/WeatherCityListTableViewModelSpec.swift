//
//  WeatherCityListTableViewModelSpec.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import Quick
import Nimble
import RxCocoa
import RxSwift

@testable import Weather

class WeatherCityListTableViewModelSpec: QuickSpec {
  
  override func spec() {
    describe("A WeatherCityListTableViewModel") {
      
      context("is instantiated") {
        // Given
        let weatherCityAPIClient = SuccessMockWeatherCityAPIClient()
        let weatherCityListTableViewModel: WeatherCityListTableViewModel
        
        // When
        weatherCityListTableViewModel = WeatherCityListTableViewModel(apiClient: weatherCityAPIClient)
        
        // Then
        it("should not be nil") {
          expect(weatherCityListTableViewModel).toNot(beNil())
        }
        it("should implement the following protocols") {
          expect(weatherCityListTableViewModel).to(beAKindOf(RxTableViewModelRefreshing.self))
          expect(weatherCityListTableViewModel).to(beAKindOf(RxErrorAlertViewModelSubjectProvider.self))
        }
      }
      
      describe("getWeatherCityListCellViewModels") {
        context("is successfully retrieving an array of WeatherCity") {
          // Given
          let weatherCityAPIClient = SuccessMockWeatherCityAPIClient()
          let weatherCityListTableViewModel: WeatherCityListTableViewModel = WeatherCityListTableViewModel(apiClient: weatherCityAPIClient)
          let disposeBag = DisposeBag()
          
          // When
          var weatherCityListCellViewModels: [WeatherCityListCellViewModel]!
          waitUntil(timeout: 1) { done in
            weatherCityListTableViewModel.getWeatherCityListCellViewModels()
              .subscribe(onNext: { cellViewModels in
                weatherCityListCellViewModels = cellViewModels
                done()
              }).disposed(by: disposeBag)
          }
          // Then
          it("should generate correctly the cell view models") {
            expect(weatherCityListCellViewModels).toNot(beNil())
            expect(weatherCityListCellViewModels.count).to(equal(15))
            expect(weatherCityListCellViewModels[0].cityName).to(equal("Yafran"))
            expect(weatherCityListCellViewModels[0].temperature).to(equal("9.7 °C"))
            expect(weatherCityListCellViewModels[0].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[1].cityName).to(equal("Zuwarah"))
            expect(weatherCityListCellViewModels[1].temperature).to(equal("15.4 °C"))
            expect(weatherCityListCellViewModels[1].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[2].cityName).to(equal("Sabratah"))
            expect(weatherCityListCellViewModels[2].temperature).to(equal("15.3 °C"))
            expect(weatherCityListCellViewModels[2].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[3].cityName).to(equal("Gharyan"))
            expect(weatherCityListCellViewModels[3].temperature).to(equal("11.2 °C"))
            expect(weatherCityListCellViewModels[3].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[4].cityName).to(equal("Zawiya"))
            expect(weatherCityListCellViewModels[4].temperature).to(equal("17.0 °C"))
            expect(weatherCityListCellViewModels[4].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[5].cityName).to(equal("Tripoli"))
            expect(weatherCityListCellViewModels[5].temperature).to(equal("16.0 °C"))
            expect(weatherCityListCellViewModels[5].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[6].cityName).to(equal("Tarhuna"))
            expect(weatherCityListCellViewModels[6].temperature).to(equal("17.0 °C"))
            expect(weatherCityListCellViewModels[6].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[7].cityName).to(equal("Masallatah"))
            expect(weatherCityListCellViewModels[7].temperature).to(equal("12.9 °C"))
            expect(weatherCityListCellViewModels[7].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[8].cityName).to(equal("Al Khums"))
            expect(weatherCityListCellViewModels[8].temperature).to(equal("15.2 °C"))
            expect(weatherCityListCellViewModels[8].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[9].cityName).to(equal("Zlitan"))
            expect(weatherCityListCellViewModels[9].temperature).to(equal("15.2 °C"))
            expect(weatherCityListCellViewModels[9].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[10].cityName).to(equal("Birkirkara"))
            expect(weatherCityListCellViewModels[10].temperature).to(equal("14.0 °C"))
            expect(weatherCityListCellViewModels[10].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[11].cityName).to(equal("Ragusa"))
            expect(weatherCityListCellViewModels[11].temperature).to(equal("14.5 °C"))
            expect(weatherCityListCellViewModels[11].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[12].cityName).to(equal("Pozzallo"))
            expect(weatherCityListCellViewModels[12].temperature).to(equal("14.0 °C"))
            expect(weatherCityListCellViewModels[12].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[13].cityName).to(equal("Modica"))
            expect(weatherCityListCellViewModels[13].temperature).to(equal("15.7 °C"))
            expect(weatherCityListCellViewModels[13].weatherCity).toNot(beNil())
            expect(weatherCityListCellViewModels[14].cityName).to(equal("Rosolini"))
            expect(weatherCityListCellViewModels[14].temperature).to(equal("15.6 °C"))
            expect(weatherCityListCellViewModels[14].weatherCity).toNot(beNil())
          }
        }
        
        context("is failing retrieving an array of WeatherCity") {
          // Given
          let weatherCityAPIClient = FailureMockWeatherCityAPIClient()
          let weatherCityListTableViewModel: WeatherCityListTableViewModel = WeatherCityListTableViewModel(apiClient: weatherCityAPIClient)
          let disposeBag = DisposeBag()
          
          // When
          var getWeatherCityListAlertViewModel: AlertViewModel!
          var weatherCityListCellViewModels: [WeatherCityListCellViewModel]!
          waitUntil(timeout: 1) { done in
            weatherCityListTableViewModel.errorAlertViewModelSubject.subscribe(onNext: { alertViewModel in
              getWeatherCityListAlertViewModel = alertViewModel
            }).disposed(by: disposeBag)
            
            weatherCityListTableViewModel.getWeatherCityListCellViewModels()
              .subscribe(onNext: { cellViewModels in
                weatherCityListCellViewModels = cellViewModels
                done()
              }).disposed(by: disposeBag)
          }
          // Then
          it("should send an alertViewModel to be displayed") {
            expect(weatherCityListCellViewModels).to(beEmpty())
            expect(getWeatherCityListAlertViewModel).toNot(beNil())
            expect(getWeatherCityListAlertViewModel.title).to(equal("Retrieve weather list"))
            expect(getWeatherCityListAlertViewModel.body).to(equal("The operation couldn’t be completed. ( error 400.)"))
            expect(getWeatherCityListAlertViewModel.confirmTitle).to(equal("OK"))
          }
        }
      }
    }
  }
}
