//
//  WeatherCityListCellViewModelSpec.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Weather

class WeatherCityListCellViewModelSpec: QuickSpec {
  override func spec() {
    describe("A WeatherCityListCellViewModel") {
      context("is instantiated") {
        // Given
        let weatherCity: WeatherCity = WeatherCityFixtures.defaultInstanceFromJson()
        let weatherCityListCellViewModel: WeatherCityListCellViewModel
        
        // When
        weatherCityListCellViewModel = WeatherCityListCellViewModel(weatherCity: weatherCity)
        
        // Then
        it("should not be nil") {
          expect(weatherCityListCellViewModel).toNot(beNil())
        }
        it("should have its properties correctly instantied") {
          expect(weatherCityListCellViewModel.cityName).to(equal("Cairns"))
          expect(weatherCityListCellViewModel.temperature).to(equal("300.2 °C"))
          expect(weatherCityListCellViewModel.weatherCity).toNot(beNil())
        }
      }
    }
  }
}
