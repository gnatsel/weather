//
//  MockWeatherCityAPIClient.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

@testable import Weather

struct SuccessMockWeatherCityAPIClient: WeatherCityAPIClientType {
  func getWeatherCityList() -> Observable<[WeatherCity]> {
    let response = WeatherCityListAPIResponseFixtures.defaultInstanceFromJson()
    
    return Observable.create { observer in
      observer.onNext(response.list)
      observer.onCompleted()
      return Disposables.create()
    }
  }
  func getWeatherCityDetail(from weatherCity: WeatherCity) -> Observable<WeatherCity> {
    let weatherCity = WeatherCityFixtures.defaultInstanceFromJson()
    
    return Observable.create { observer in
      observer.onNext(weatherCity)
      observer.onCompleted()
      return Disposables.create()
    }
  }
}

struct FailureMockWeatherCityAPIClient: WeatherCityAPIClientType {
  func getWeatherCityList() -> Observable<[WeatherCity]> {
    let error = NSError(domain: "", code: 400, userInfo: nil)
    
    return Observable.create { observer in
      observer.onError(error)
      return Disposables.create()
    }
  }
  func getWeatherCityDetail(from weatherCity: WeatherCity) -> Observable<WeatherCity> {
    let error = NSError(domain: "", code: 400, userInfo: nil)
    
    return Observable.create { observer in
      observer.onError(error)
      return Disposables.create()
    }
  }
}
