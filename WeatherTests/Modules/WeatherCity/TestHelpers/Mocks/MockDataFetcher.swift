//
//  MockDataFetcher.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

@testable import Weather

struct MockDataFetcher: DataFetching {
  
  let requestHandler: ((URLRequest) -> Observable<Data>)
  
  func data(request: URLRequest) -> Observable<Data> {
    return requestHandler(request)
  }
}
