//
//  WeatherCityFixtures.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation

@testable import Weather

class WeatherCityFixtures {
  static func defaultInstance() -> WeatherCity {
    return WeatherCity(id: 0,
                       name: "",
                       coordinates: WeatherCity.Coordinates(latitude: 0, longitude: 0),
                       main: WeatherCity.Parameters(temperature: 0, temperatureMinimum: 0, temperatureMaximum: 0, pressure: 0, humidity: 0, seaLevel: nil, groundLevel: nil),
                       dateReceivingTime: 0,
                       wind: WeatherCity.Wind(speed: 0, direction: 0),
                       snow: nil,
                       rain: nil,
                       clouds: WeatherCity.Clouds(cloudiness: 0),
                       weather: [WeatherCity.Weather](),
                       sun: nil)
  }
  
  static func defaultInstanceFromJson() -> WeatherCity {
    return try! JSONDecoder().decode(WeatherCity.self, from: WeatherCityFixtures.defaultDataFromJson())
  }
  
  static func defaultDataFromJson() -> Data {
    let url = Bundle(for: self).url(forResource: "WeatherCity", withExtension: "json")!
    return try! Data(contentsOf:url, options: .mappedIfSafe)
  }
}
