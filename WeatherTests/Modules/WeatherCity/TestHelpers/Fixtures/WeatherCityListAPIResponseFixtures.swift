//
//  WeatherCityListAPIResponseFixtures.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation

@testable import Weather

class WeatherCityListAPIResponseFixtures {
  static func defaultInstance() -> WeatherCityListAPIResponse {
    return WeatherCityListAPIResponse(cod: "", calctime: 0, cnt: 0, list: [WeatherCity]())
  }
  
  static func defaultInstanceFromJson() -> WeatherCityListAPIResponse {
    return try! JSONDecoder().decode(WeatherCityListAPIResponse.self, from: WeatherCityListAPIResponseFixtures.defaultDataFromJson())
  }
  
  static func defaultDataFromJson() -> Data {
    let url = Bundle(for: self).url(forResource: "WeatherCityListAPIResponse", withExtension: "json")!
    return try! Data(contentsOf:url, options: .mappedIfSafe)
  }
}
