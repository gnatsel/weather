//
//  AppCoordinatorSpec.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Weather

class AppCoordinatorSpec: QuickSpec {
  
  var appCoordinator: AppCoordinator!
  
  override func spec() {
    describe("An AppCoordinator") {
      
      context("is instantiated") {
        beforeEach {
          self.appCoordinator = AppCoordinator()
        }
        
        it("should not be nil") {
          expect(self.appCoordinator).toNot(beNil())
        }
        
        it("has been correctly instantiated") {
          expect(self.appCoordinator.weatherCityCoordinator).toNot(beNil())
        }
      }
      
      describe("prepare window") {
        context("with a valid rootViewController") {
          // Given
          self.appCoordinator = AppCoordinator()
          let window: UIWindow = UIWindow()
          let storyboard = UIStoryboard(name: "WeatherCityList", bundle: Bundle(for: WeatherCityListTableViewController.self))
          window.rootViewController = storyboard.instantiateInitialViewController()
          
          // When
          self.appCoordinator.prepare(window: window)
          
          // Then
          it("should set the view model and the coordinator of the instance of WeatherCityListTableViewController") {
            let navigationController = window.rootViewController as? UINavigationController
            let weatherCityListTableViewController = navigationController?.viewControllers.first as? WeatherCityListTableViewController
            expect(navigationController).toNot(beNil())
            expect(weatherCityListTableViewController).toNot(beNil())
            expect(weatherCityListTableViewController?.coordinator).toNot(beNil())
            expect(weatherCityListTableViewController?.viewModel).toNot(beNil())
          }
        }
      }
    }
  }
}
