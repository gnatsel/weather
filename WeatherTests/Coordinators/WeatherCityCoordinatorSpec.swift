//
//  WeatherCityCoordinatorSpec.swift
//  WeatherTests
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Weather

class WeatherCityCoordinatorSpec: QuickSpec {
  
  var weatherCityCoordinator: WeatherCityCoordinator!
  
  override func spec() {
    describe("A WeatherCityCoordinator") {
      
      context("is instantiated") {
        self.weatherCityCoordinator = WeatherCityCoordinator()
        
        it("should not be nil") {
          expect(self.weatherCityCoordinator).toNot(beNil())
        }
      }
      
      describe("show weather city detail") {
        // Given
        self.weatherCityCoordinator = WeatherCityCoordinator()
        let storyboard = UIStoryboard(name: "WeatherCityList", bundle: Bundle(for: WeatherCityListTableViewController.self))
        let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        let weatherCityListTableViewController = navigationController.viewControllers.first as! WeatherCityListTableViewController
        let weatherCity = WeatherCity(id: 0,
                                      name: "",
                                      coordinates: WeatherCity.Coordinates(latitude: 0, longitude: 0),
                                      main: WeatherCity.Parameters(temperature: 0, temperatureMinimum: 0, temperatureMaximum: 0, pressure: 0, humidity:0, seaLevel: nil, groundLevel: nil),
                                      dateReceivingTime: 0,
                                      wind: WeatherCity.Wind(speed: 0, direction: 0),
                                      snow: nil,
                                      rain: nil,
                                      clouds: WeatherCity.Clouds(cloudiness: 0),
                                      weather: [WeatherCity.Weather](),
                                      sun: nil)
        var weatherCityListDetailViewController: WeatherCityDetailTableViewController!
        
        // When
        self.weatherCityCoordinator.perform(segueCoordinated: .showWeatherDetail(weatherCity: weatherCity), sender: weatherCityListTableViewController) {
          weatherCityListDetailViewController = $0 as? WeatherCityDetailTableViewController
        }
        
        // Then
        it("should set the view model and the coordinator of the instance of WeatherCityDetailTableViewController") {
          expect(weatherCityListDetailViewController).toEventuallyNot(beNil())
          expect(weatherCityListDetailViewController?.coordinator).toEventuallyNot(beNil())
          expect(weatherCityListDetailViewController?.viewModel).toEventuallyNot(beNil())
        }
      }
    }
  }
  
}
