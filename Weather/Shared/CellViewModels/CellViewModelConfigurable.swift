//
//  CellViewModelConfigurable.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation

/// CellViewModeling
protocol CellViewModeling {}

protocol CellViewModelConfigurable {
  
  associatedtype ViewModel: CellViewModeling
  
  func configureWith(viewModel: ViewModel)
}
