//
//  RxTableViewRefreshing.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol RxTableViewRefreshing {
  associatedtype ViewModel: RxTableViewModelRefreshing
  
  var viewModel: ViewModel! { get set }
  
  func refreshTable()
}

extension RxTableViewRefreshing where Self: RxTableViewController {
  internal func configureRefreshControl() {
    let refreshControl = UIRefreshControl()
    self.refreshControl = refreshControl
    
    refreshControl.rx
      .controlEvent(.valueChanged)
      .asDriver()
      .drive(onNext: { [unowned self] _ in
        self.refreshTable()
      })
      .disposed(by: disposeBag)
    
    viewModel.isRefreshing
      .bind(to: refreshControl.rx.isRefreshing)
      .disposed(by: disposeBag)
  }
}

protocol RxTableViewModelRefreshing {
  var isRefreshing: Observable<Bool> { get }
}
