//
//  RxErrorAlertDisplaying.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol RxErrorAlertViewModelSubjectProvider {
  var errorAlertViewModelSubject: PublishSubject<AlertViewModel> { get }
}

protocol RxErrorAlertDisplaying: AlertDisplaying {
  associatedtype ViewModel: RxErrorAlertViewModelSubjectProvider
  
  var viewModel: ViewModel! { get set }
}

extension RxErrorAlertDisplaying where Self: (UIViewController & AlertDisplaying & RxDisposing) {
  func configureErrorAlertViewModelDisplay() {
    viewModel.errorAlertViewModelSubject.subscribe(onNext: { [weak self] alertViewModel in
      self?.showAlert(viewModel: alertViewModel)
    }).disposed(by: disposeBag)
  }
}
