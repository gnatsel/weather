//
//  RxTableViewController.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import UIKit
import RxSwift

class RxTableViewController: UITableViewController, RxDisposing {
  var disposeBag: DisposeBag = DisposeBag()
}

