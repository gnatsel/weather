//
//  RxDisposing.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import RxSwift

protocol RxDisposing {
  var disposeBag: DisposeBag { get }
}
