//
//  Constants.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation

struct Constants {
  
  struct API {
    static let baseURL: URL = URL(string: "https://samples.openweathermap.org/")!
    static let weatherAPIAppIdParameter: String = "appid=b1b15e88fa797225412429c1c50c122a1"
  }
  
}
