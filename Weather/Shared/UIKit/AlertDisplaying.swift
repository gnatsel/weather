//
//  AlertDisplaying.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import UIKit

struct AlertViewModel {
  let title: String
  let body: String
  let confirmTitle: String
  let confirmAction: () -> ()
}

protocol AlertDisplaying {}

extension AlertDisplaying where Self: UIViewController {
  func showAlert(viewModel: AlertViewModel, completion: (() ->())? = nil) {
    let controller = UIAlertController(title: viewModel.title,
                                       message: viewModel.body,
                                       preferredStyle: .alert)
    controller.addAction(
      UIAlertAction(title: viewModel.confirmTitle, style: .cancel) { _ in
        viewModel.confirmAction()
      }
    )
    present(controller, animated: true, completion: completion)
  }
}

