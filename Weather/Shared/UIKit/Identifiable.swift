//
//  Identifiable.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import UIKit


protocol Identifiable {}

extension Identifiable where Self: UIViewController {
  static var defaultStoryboardNavigationSegueIdentifier: String { return String(describing: self).replacingOccurrences(of: "ViewController", with: "NavigationControllerStoryboardSegueIdentifier") }
  static var defaultStoryboardSegueIdentifier: String { return String(describing: self) + "StoryboardSegueIdentifier" }
  static var defaultStoryboardUnwindSegueIdentifier: String { return String(describing: self) + "StoryboardUnwindSegueIdentifier" }
}

extension Identifiable where Self: UITableViewCell {
  static var defaultReuseIdentifier: String { return String(describing: self) + "ReuseIdentifier" }
}

extension Identifiable where Self: UICollectionViewCell {
  static var defaultReuseIdentifier: String { return String(describing: self) + "ReuseIdentifier" }
}
