//
//  DataFetching.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol DataFetching {
  func data(request: URLRequest) -> Observable<Data>
}

extension Reactive: DataFetching where Base: URLSession {}
