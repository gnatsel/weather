//
//  APIClientType.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol PathRepresentable {
  var pathValue: String { get }
}

enum APIClientError<Path: PathRepresentable>: Error {
  case invalid(path: Path, baseURL: URL)
}

protocol APIClientType {
  associatedtype Path: PathRepresentable
  var baseURL: URL { get }
  var dataFetcher: DataFetching { get }
}

extension APIClientType {
  
  private var backgroundScheduler: ImmediateSchedulerType {
    let operationQueue = OperationQueue()
    operationQueue.maxConcurrentOperationCount = 2
    operationQueue.qualityOfService = QualityOfService.userInitiated
    return OperationQueueScheduler(operationQueue: operationQueue)
  }
  
  func get<T: Decodable>(path: Path, retry: Int = 3) -> Observable<T> {
    guard let url = URL(string: path.pathValue, relativeTo: self.baseURL) else {
      return Observable.error(APIClientError.invalid(path: path, baseURL: self.baseURL))
    }
    var request = URLRequest(url: url)
    request.httpMethod = RequestMethod.get.rawValue
    
    return dataFetcher
      .data(request: request)
      .retry(retry)
      .observeOn(self.backgroundScheduler)
      .map{ data -> T in
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    }
    
  }
}

/// HTTP methods supported
private enum RequestMethod: String {
  case get
  case post
  case put
  case patch
  case delete
}
