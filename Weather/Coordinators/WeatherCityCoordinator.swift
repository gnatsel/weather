//
//  WeatherCityCoordinator.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import Perform

private extension Segue {
  static var showWeatherCityDetail: Segue<WeatherCityDetailTableViewController> {
    return .init(identifier: WeatherCityDetailTableViewController.defaultStoryboardSegueIdentifier)
  }
}

final class WeatherCityCoordinator: Coordinating {
  
  enum SegueCoordinator: SegueCoordinating {
    case showWeatherDetail(weatherCity: WeatherCity)
  }
  
  func perform(segueCoordinator: SegueCoordinator, sender: UIViewController) {
    perform(segueCoordinated: segueCoordinator, sender: sender, performHandler: nil)
  }
  
  /// This method should only be used for unit testing purpose
  func perform(segueCoordinated: SegueCoordinator, sender: UIViewController, performHandler:((UIViewController) -> Void)?) {
    switch segueCoordinated {
    case .showWeatherDetail(let weatherCity):
      sender.perform(.showWeatherCityDetail) { [weak self] weatherCityDetailViewController in
        weatherCityDetailViewController.coordinator = self
        weatherCityDetailViewController.viewModel = WeatherCityDetailTableViewModel(weatherCity: weatherCity)
        performHandler?(weatherCityDetailViewController)
      }
    }
  }
}
