//
//  AppCoordinator.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import UIKit

final class AppCoordinator {
  let weatherCityCoordinator: WeatherCityCoordinator = WeatherCityCoordinator()
  
  func prepare(window: UIWindow?) {
    guard let rootViewController = window?.rootViewController as? UINavigationController,
      let weatherCityListTableViewController = rootViewController.viewControllers.first as? WeatherCityListTableViewController else { return }
    weatherCityListTableViewController.viewModel = WeatherCityListTableViewModel()
    weatherCityListTableViewController.coordinator = weatherCityCoordinator
  }
}
