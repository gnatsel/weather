//
//  Coordinating.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import UIKit

protocol SegueCoordinating {}

protocol Coordinating: class {
  associatedtype SegueCoordinator: SegueCoordinating
  
  func perform(segueCoordinator: SegueCoordinator, sender: UIViewController)
}
