//
//  Coordinatable.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation

protocol Coordinatable: class {
  associatedtype Coordinator: Coordinating
  var coordinator: Coordinator! { get set }
}
