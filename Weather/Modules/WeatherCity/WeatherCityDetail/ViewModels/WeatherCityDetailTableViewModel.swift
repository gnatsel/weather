//
//  WeatherCityDetailTableViewModel.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class WeatherCityDetailTableViewModel {
  private var weatherCity: WeatherCity
  private let apiClient: WeatherCityAPIClientType
  private let activityIndicator: ActivityIndicator = ActivityIndicator()
  
  private lazy var dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = Constants.dateFormat.rawValue
    return dateFormatter
  }()
  
  let isRefreshing: Observable<Bool>
  let errorAlertViewModelSubject: PublishSubject<AlertViewModel> = PublishSubject<AlertViewModel>()
  
  init(weatherCity: WeatherCity, apiClient: WeatherCityAPIClientType = WeatherCityAPIClient()) {
    self.weatherCity = weatherCity
    self.apiClient = apiClient
    isRefreshing = activityIndicator.asObservable()
  }
  
  func getWeatherCityDetailCellViewModels() -> Observable<[WeatherCityDetailCellViewModel]> {
    
    return apiClient.getWeatherCityDetail(from: weatherCity)
      .map{ [weak self] weatherCity -> [WeatherCityDetailCellViewModel] in
        guard let `self` = self else { return [WeatherCityDetailCellViewModel]() }
        self.updateWeatherCity(with: weatherCity)
        
        var cellViewModels: [WeatherCityDetailCellViewModel?] = [WeatherCityDetailCellViewModel?]()
        
        // City info
        cellViewModels.append(self.cityInfoCellViewModel())
        cellViewModels.append(self.sunCellViewModel())
        
        // Main weather parameters
        let weatherMainCellViewModel = WeatherCityDetailCellViewModel(infoName: Constants.weatherParameterName.rawValue,
                                                                      infoValue: self.weatherDescription())
        cellViewModels.append(weatherMainCellViewModel)
        
        // Weather list
        weatherCity.weather.forEach{ weather in
          let weatherCellViewModel = WeatherCityDetailCellViewModel(infoName: weather.name,
                                                                    infoValue: weather.description)
          cellViewModels.append(weatherCellViewModel)
        }
        
        cellViewModels.append(self.windCellViewModel())
        
        // Cloud
        let cloudCellViewModel = WeatherCityDetailCellViewModel(infoName: Constants.cloudinessName.rawValue,
                                                                infoValue: "\(weatherCity.clouds.cloudiness)%")
        cellViewModels.append(cloudCellViewModel)
        
        // Rain
        cellViewModels.append(self.rainCellViewModel())
        
        // Snow
        cellViewModels.append(self.snowCellViewModel())
        
        return cellViewModels.compactMap{ $0 }
      }
      .trackActivity(activityIndicator)
      .observeOn(MainScheduler.instance)
      .asDriver(onErrorRecover: { [weak self] error in
        let driver = Driver.just([WeatherCityDetailCellViewModel]())
        guard let `self` = self else { return driver }
        self.errorAlertViewModelSubject.onNext(AlertViewModel(title: Constants.retrieveWeatherCityDetailAlertViewModelTitle.rawValue,
                                                              body: error.localizedDescription,
                                                              confirmTitle: Constants.retrieveWeatherCityDetailAlertViewModelConfirmButtonTitle.rawValue) { })
        return driver
      }).asObservable()
  }
  
  private func updateWeatherCity(with weatherCity: WeatherCity) {
    /// !!! We should use the retrieved weather city instead of the one from the list
    /// !!! However there is currently an issue with openweathermap.org:
    /// !!! it only provide the value for the city Cairns
    let newWeatherCity = WeatherCity(id: self.weatherCity.id,
                                     name: self.weatherCity.name,
                                     coordinates: self.weatherCity.coordinates,
                                     main: self.weatherCity.main,
                                     dateReceivingTime: self.weatherCity.dateReceivingTime,
                                     wind: self.weatherCity.wind,
                                     snow: self.weatherCity.snow,
                                     rain: self.weatherCity.rain,
                                     clouds: self.weatherCity.clouds,
                                     weather: self.weatherCity.weather,
                                     sun: weatherCity.sun)
    
    self.weatherCity = newWeatherCity
  }
  
  /// Mark - Cell view models creation
  
  private func cityInfoCellViewModel() -> WeatherCityDetailCellViewModel {
    let updateDate = Date(timeIntervalSince1970: weatherCity.dateReceivingTime)
    
    let dateString = dateFormatter.string(from: updateDate)
    return WeatherCityDetailCellViewModel(infoName: "\(Constants.cityInfoName.rawValue)",
      infoValue: "\(weatherCity.name)\nlatitude: \(weatherCity.coordinates.latitude), longitude: \(weatherCity.coordinates.longitude)\ndate received: \(dateString)")
  }
  
  private func sunCellViewModel() -> WeatherCityDetailCellViewModel? {
    guard let sun = weatherCity.sun else { return nil }
    let sunriseDate = Date(timeIntervalSince1970: sun.sunrise)
    let sunsetDate = Date(timeIntervalSince1970: sun.sunset)
    
    let sunInfoValue = """
    \(Constants.sunriseName.rawValue): \(dateFormatter.string(from: sunriseDate))
    \(Constants.sunsetName.rawValue): \(dateFormatter.string(from: sunsetDate))
    """
    return WeatherCityDetailCellViewModel(infoName: Constants.sunName.rawValue,
                                          infoValue: sunInfoValue)
    
  }
  
  private func weatherDescription() -> String {
    func roundedTemperatureString(from temperature: Float) -> String {
      let roundedTemperature = (temperature * 10).rounded() / 10
      return "\(roundedTemperature) °C"
    }
    
    func roundedPressureString(from pressure: Float?) -> String? {
      guard let pressure = pressure else { return nil }
      let roundedPressure = pressure.rounded()
      return "\(roundedPressure) hPa"
    }
    
    return  [
      Constants.weatherTemperatureName.rawValue: roundedTemperatureString(from: weatherCity.main.temperature),
      Constants.weatherTemperatureMinimumName.rawValue: roundedTemperatureString(from: weatherCity.main.temperatureMinimum),
      Constants.weatherTemperatureMaximumName.rawValue: roundedTemperatureString(from: weatherCity.main.temperatureMaximum),
      Constants.weatherPressureName.rawValue: roundedPressureString(from: weatherCity.main.pressure),
      Constants.weatherHumidityName.rawValue: "\(weatherCity.main.humidity)%",
      Constants.weatherSeaLevelName.rawValue: roundedPressureString(from: weatherCity.main.seaLevel),
      Constants.weatherGroundLevelName.rawValue: roundedPressureString(from: weatherCity.main.groundLevel)
      ]
      .reduce(into: [String: String]()) { (result, keyValueTuple) in
        guard let value = keyValueTuple.value else { return }
        result.updateValue(value, forKey: keyValueTuple.key)
      }.map { "\($0.key): \($0.value)" }
      .joined(separator: "\n")
  }
  
  private func windCellViewModel() -> WeatherCityDetailCellViewModel {
    let windInfoValue = """
    \(Constants.windDirectionName.rawValue): \(weatherCity.wind.direction) °
    \(Constants.windSpeedName.rawValue): \(weatherCity.wind.speed) m/s
    """
    return WeatherCityDetailCellViewModel(infoName: Constants.windName.rawValue,
                                          infoValue: windInfoValue)
  }
  
  private func rainCellViewModel() -> WeatherCityDetailCellViewModel? {
    guard let rain = weatherCity.rain else { return nil }
    return WeatherCityDetailCellViewModel(infoName: Constants.rainName.rawValue,
                                          infoValue: "\(rain.lastThreeHoursVolume) mm")
  }
  
  private func snowCellViewModel() -> WeatherCityDetailCellViewModel? {
    guard let snow = weatherCity.snow else { return nil }
    return WeatherCityDetailCellViewModel(infoName: Constants.snowName.rawValue,
                                          infoValue: "\(snow.lastThreeHoursVolume) mm")
  }
}

extension WeatherCityDetailTableViewModel: RxTableViewModelRefreshing {}
extension WeatherCityDetailTableViewModel: RxErrorAlertViewModelSubjectProvider {}
extension WeatherCityDetailTableViewModel {
  fileprivate enum Constants: String {
    case cityInfoName = "City Info"
    case cloudinessName = "Cloudiness"
    case dateFormat = "dd-MM-yyyy hh:mm:ss a"
    case weatherHumidityName = "Humidity"
    case weatherParameterName = "Weather parameters"
    case weatherTemperatureName = "Current temperature"
    case weatherTemperatureMinimumName = "Minimum daily temperature"
    case weatherTemperatureMaximumName = "Maximum daily temperature"
    case weatherPressureName = "Atmospheric Pressure"
    case weatherSeaLevelName = "Atmospheric pressure on the sea level"
    case weatherGroundLevelName = "Atmospheric pressure on the ground level"
    case windName = "Wind"
    case windDirectionName = "Direction"
    case windSpeedName = "Speed"
    case rainName = "Precipitation volume for last 3 hours"
    case retrieveWeatherCityDetailAlertViewModelTitle = "Retrieve weather detail"
    case retrieveWeatherCityDetailAlertViewModelConfirmButtonTitle = "OK"
    case snowName = "Snow volume for last 3 hours"
    case sunName = "Sun"
    case sunriseName = "Sunrise"
    case sunsetName = "Sunset"
  }
}
