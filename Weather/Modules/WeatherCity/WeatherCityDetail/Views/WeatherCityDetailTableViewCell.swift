//
//  WeatherCityDetailTableViewCell.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import UIKit

final class WeatherCityDetailTableViewCell: UITableViewCell {
  
  @IBOutlet var infoNameLabel: UILabel!
  @IBOutlet var infoValueLabel: UILabel!
  
  func configureWith(viewModel: WeatherCityDetailCellViewModel) {
    infoNameLabel.text = viewModel.infoName
    infoValueLabel.text = viewModel.infoValue
  }
}
extension WeatherCityDetailTableViewCell: Identifiable {}
extension WeatherCityDetailTableViewCell: CellViewModelConfigurable {}
