//
//  WeatherCityDetailTableViewController.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import UIKit

final class WeatherCityDetailTableViewController: RxTableViewController {
  
  var coordinator: WeatherCityCoordinator!
  var viewModel: WeatherCityDetailTableViewModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    assert(coordinator != nil, "property `coordinator` needs to be set")
    assert(viewModel != nil, "property `viewModel` needs to be set")
    
    configureErrorAlertViewModelDisplay()
    configureRefreshControl()
    refreshTable()
  }
  
  internal func refreshTable() {
    self.tableView.delegate = nil
    self.tableView.dataSource = nil
    viewModel.getWeatherCityDetailCellViewModels()
      .bind(to: tableView.rx.items(cellIdentifier: WeatherCityDetailTableViewCell.defaultReuseIdentifier,
                                   cellType: WeatherCityDetailTableViewCell.self))
      { (row, cellViewModel, cell) in
        cell.configureWith(viewModel: cellViewModel)
      }.disposed(by: disposeBag)
  }
  
}

extension WeatherCityDetailTableViewController: Coordinatable {}
extension WeatherCityDetailTableViewController: Identifiable {}
extension WeatherCityDetailTableViewController: RxTableViewRefreshing {}
extension WeatherCityDetailTableViewController: RxErrorAlertDisplaying {}
