//
//  WeatherCityListTableViewController.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Perform

final class WeatherCityListTableViewController: RxTableViewController {
  
  var coordinator: WeatherCityCoordinator!
  var viewModel: WeatherCityListTableViewModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    assert(coordinator != nil, "property `coordinator` needs to be set")
    assert(viewModel != nil, "property `viewModel` needs to be set")
    
    configureErrorAlertViewModelDisplay()
    configureRefreshControl()
    configureCellSelection()
    refreshTable()
  }
  
  internal func refreshTable() {
    self.tableView.delegate = nil
    self.tableView.dataSource = nil
    
    viewModel.getWeatherCityListCellViewModels()
      .bind(to: tableView.rx.items(cellIdentifier: WeatherCityListTableViewCell.defaultReuseIdentifier,
                                   cellType: WeatherCityListTableViewCell.self))
      { (row, cellViewModel, cell) in
        cell.configureWith(viewModel: cellViewModel)
      }.disposed(by: disposeBag)
  }
  
  private func configureCellSelection() {
    tableView.rx
      .modelSelected(WeatherCityListCellViewModel.self)
      .subscribe(onNext: { [unowned self] cellViewModel in
        self.coordinator.perform(segueCoordinator: .showWeatherDetail(weatherCity: cellViewModel.weatherCity), sender: self)
      }).disposed(by: disposeBag)
  }
}

extension WeatherCityListTableViewController: Coordinatable {}
extension WeatherCityListTableViewController: RxTableViewRefreshing {}
extension WeatherCityListTableViewController: RxErrorAlertDisplaying {}
