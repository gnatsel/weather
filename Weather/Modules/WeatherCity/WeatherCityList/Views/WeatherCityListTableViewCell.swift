//
//  WeatherCityListTableViewCell.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import UIKit

final class WeatherCityListTableViewCell: UITableViewCell {
  
  @IBOutlet var cityLabel: UILabel!
  @IBOutlet var temperatureLabel: UILabel!

  func configureWith(viewModel: WeatherCityListCellViewModel) {
    cityLabel.text = viewModel.cityName
    temperatureLabel.text = viewModel.temperature
  }
}

extension WeatherCityListTableViewCell: Identifiable {}
extension WeatherCityListTableViewCell: CellViewModelConfigurable {}
