//
//  WeatherCityListTableViewModel.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class WeatherCityListTableViewModel {
  private let apiClient: WeatherCityAPIClientType
  private let activityIndicator = ActivityIndicator()
  
  let isRefreshing: Observable<Bool>
  let errorAlertViewModelSubject: PublishSubject<AlertViewModel> = PublishSubject<AlertViewModel>()
  
  init(apiClient: WeatherCityAPIClientType = WeatherCityAPIClient()) {
    self.apiClient = apiClient
    isRefreshing = activityIndicator.asObservable()
  }
  
  
  func getWeatherCityListCellViewModels() -> Observable<[WeatherCityListCellViewModel]> {
    return apiClient.getWeatherCityList()
      .map{ $0.map { weatherCity in WeatherCityListCellViewModel(weatherCity: weatherCity) } }
      .trackActivity(activityIndicator)
      .observeOn(MainScheduler.instance)
      .asDriver(onErrorRecover: { [weak self] error in
        let driver = Driver.just([WeatherCityListCellViewModel]())
        guard let `self` = self else { return driver }
        self.errorAlertViewModelSubject.onNext(AlertViewModel(title: Constants.retrieveWeatherCityListAlertViewModelTitle.rawValue,
                                                              body: error.localizedDescription,
                                                              confirmTitle: Constants.retrieveWeatherCityListAlertViewModelConfirmButtonTitle.rawValue) { })
        return driver
      }).asObservable()
  }
}

extension WeatherCityListTableViewModel: RxTableViewModelRefreshing {}
extension WeatherCityListTableViewModel: RxErrorAlertViewModelSubjectProvider {}
extension WeatherCityListTableViewModel {
  fileprivate enum Constants: String {
    case retrieveWeatherCityListAlertViewModelTitle = "Retrieve weather list"
    case retrieveWeatherCityListAlertViewModelConfirmButtonTitle = "OK"
  }
}
