//
//  WeatherCityListCellViewModel.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation

struct WeatherCityListCellViewModel: CellViewModeling {
  let weatherCity: WeatherCity
  
  var cityName: String { return weatherCity.name }
  var temperature: String {
    let roundedTemperature = (weatherCity.main.temperature * 10).rounded() / 10
    return "\(roundedTemperature) °C" }
  
}
