//
//  WeatherCityAPIClient.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol WeatherCityAPIClientType {
  func getWeatherCityList() -> Observable<[WeatherCity]>
  func getWeatherCityDetail(from weatherCity: WeatherCity) -> Observable<WeatherCity>
}

struct WeatherCityAPIClient: APIClientType {
  typealias Path = WeatherCityPath
  
  let baseURL: URL
  let dataFetcher: DataFetching
  
  init(baseURL: URL = Constants.API.baseURL, dataFetcher: DataFetching = URLSession.shared.rx) {
    self.baseURL = baseURL
    self.dataFetcher = dataFetcher
  }
  
  enum WeatherCityPath: PathRepresentable {
    case getWeatherCityList
    case getWeatherCityDetail(weatherCity: WeatherCity)

    private enum WeatherCityScheme: String {
      case getWeatherCityList = "/data/2.5/box/city?bbox=12,32,15,37,10&%@"
      case getWeatherCityDetail = "/data/2.5/weather?id=%@&%@"
    }
    
    var pathValue: String {
      switch self {
      case .getWeatherCityList:
        return String(format: WeatherCityScheme.getWeatherCityList.rawValue, arguments: [Constants.API.weatherAPIAppIdParameter])
      case .getWeatherCityDetail(let weatherCity):
        return String(format: WeatherCityScheme.getWeatherCityDetail.rawValue,
                      arguments: [ "\(weatherCity.id)",
                        Constants.API.weatherAPIAppIdParameter])
      }
    }
  }
}

extension WeatherCityAPIClient: WeatherCityAPIClientType {
  func getWeatherCityList() -> Observable<[WeatherCity]> {
    let response: Observable<WeatherCityListAPIResponse> = get(path: .getWeatherCityList)
    return response
      .map{ $0.list }
  }
  
  func getWeatherCityDetail(from weatherCity: WeatherCity) -> Observable<WeatherCity> {
    return get(path: .getWeatherCityDetail(weatherCity: weatherCity))
  }
}
