//
//  WeatherCityListAPIResponse.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation

struct WeatherCityListAPIResponse: Codable {
  let cod: String
  let calctime: Float
  let cnt: Int
  let list: [WeatherCity]
}
