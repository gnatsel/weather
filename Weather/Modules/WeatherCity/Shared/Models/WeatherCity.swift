//
//  WeatherCity.swift
//  Weather
//
//  Created by Olivier Lestang on 17/9/18.
//  Copyright © 2018 gnatsel. All rights reserved.
//

import Foundation

// Codable model from: https://openweathermap.org/weather-data

struct WeatherCity: Codable {
  let id: Int
  let name: String
  let coordinates: Coordinates
  let main: Parameters
  let dateReceivingTime: Double
  let wind: Wind
  let snow: Snow?
  let rain: Rain?
  let clouds: Clouds
  let weather: [Weather]
  let sun: Sun?
  
  enum CodingKeys: String, CodingKey {
    case id
    case name
    case coordinates = "coord"
    case main
    case dateReceivingTime = "dt"
    case wind
    case snow
    case rain
    case clouds
    case weather
    case sun = "sys"
  }
}

// mark - Internal structs
extension WeatherCity {
  struct Coordinates: Codable {
    let latitude: Double
    let longitude: Double
    
    enum CodingKeys: String, CodingKey {
      case latitude = "lat"
      case longitude = "lon"
    }
  }
  
  struct Parameters: Codable {
    let temperature: Float
    let temperatureMinimum: Float
    let temperatureMaximum: Float
    let pressure: Float
    let humidity: Float
    let seaLevel: Float?
    let groundLevel: Float?
    
    enum CodingKeys: String, CodingKey {
      case temperature = "temp"
      case temperatureMinimum = "temp_min"
      case temperatureMaximum = "temp_max"
      case pressure
      case humidity
      case seaLevel = "sea_level"
      case groundLevel = "grnd_level"
    }
  }
  
  struct Wind: Codable {
    let speed: Float
    let direction: Float
    
    enum CodingKeys: String, CodingKey {
      case speed
      case direction = "deg"
    }
  }
  
  struct Rain: Codable {
    let lastThreeHoursVolume: Float
    
    enum CodingKeys: String, CodingKey {
      case lastThreeHoursVolume = "3h"
    }
  }
  
  struct Snow: Codable {
    let lastThreeHoursVolume: Float
    
    enum CodingKeys: String, CodingKey {
      case lastThreeHoursVolume = "3h"
    }
  }
  
  struct Clouds: Codable {
    let cloudiness: Float
    
    enum CodingKeys: String, CodingKey {
      case cloudiness = "all"
    }
  }
  
  struct Weather: Codable {
    let id: Int
    let name: String
    let description: String
    let icon: String
    
    enum CodingKeys: String, CodingKey {
      case id
      case name = "main"
      case description
      case icon
    }
  }
  
  struct Sun: Codable {
    let country: String
    let sunrise: Double
    let sunset: Double
  }
}
